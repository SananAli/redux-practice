import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from './store/counterSlice';
import { getAllUsersMiddleAction,  } from "./redux-thunk/users";
function App() {
  const dispatch = useDispatch();
  let allUsers = useSelector((state)=>{
    return state.users.allUsers;
  });
  
  useEffect(()=>{
    dispatch(getAllUsersMiddleAction());
  }, [dispatch]);
  
  return (
    <div className="App">
      <div>
        {/* <button
          aria-label="Increment value"
          onClick={() => dispatch(()=>{
            return {task: "GET"}
          })}
        >
          Load Users
        </button>
        <button
          aria-label="Increment value"
          onClick={() => dispatch(()=>{
            return {task: "DELETE"}
          })}
        >
          Reset Users
        </button> */}
        
        {allUsers.length==0? <p>Please Wait.. Users are being fetched..</p>: allUsers.map(user =>(<p key={user.id}>{user.username}</p>))}
        
      </div>
    </div>
  );
}

export default App;
