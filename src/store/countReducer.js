const countReducer = (state = 0, action)=>{
    if(action.type=="INCREMENT"){
        let newState = state+1;
        state = newState;
        return state;
    }
    else if(action.type=="DECREMENT"){
        let newState = state-1;
        state = newState;
        return state;
    }
    return state;
}

export default countReducer;