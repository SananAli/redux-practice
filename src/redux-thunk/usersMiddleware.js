import { apiCallInitialized } from "./api";
const usersMiddleware = ({dispatch, getState}) => (next) => (action)=>{
    const {url, onSuccess, onFailure, data} = action.payload
    if(action.type === apiCallInitialized.type){
        fetch(url)
        .then(response => response.json())
        .then(json => {
            dispatch({type:onSuccess, payload:json});
        }).catch((e)=>{
            dispatch({type:onFailure, payload:e});
        });
        return;
    }
    next(action);
    
}
export default usersMiddleware;