import { createAction } from "@reduxjs/toolkit";
export const apiCallInitialized = createAction("api/callInitialized");
export const createAPIPayload = ({url, onSuccess, onFailure, data})=>({
    url,
    onSuccess,
    onFailure,
    data
});