import { createSlice, createSelector } from "@reduxjs/toolkit";
import { apiCallInitialized, createAPIPayload} from "./api";
const usersSlice = createSlice({
    initialState : {
        loading: true,
        allUsers : [],
    },
    name: "users",
    reducers: {
        usersLoaded: (users, action)=>{
            users.loading = false;
            users.allUsers = action.payload;
        },
        usersLoadingFailed: (users, action)=>{
            console.log("Error in Fetching Data", action.payload);
        }
        
    }
});

// middleware actions 
export const getAllUsersMiddleAction = (users) => async (dispatch, getState)=>{
        try{
            return await dispatch(
                apiCallInitialized(
                    createAPIPayload({
                        url: "https://jsonplaceholder.typicode.com/users",
                        onSuccess: usersLoaded.type,
                        onFailure: usersLoadingFailed.type,
                        data:[]
                    })
                )
            )
        }      
        catch(e){
            console.log(e);
        }       
}


// action creators
export const getAllUsers = ()=>{
    createSelector(
        (state)=> state.users.allUsers, 
        allusers=>allusers
    )
}

// export all actions 
export const { usersLoaded, usersLoadingFailed } = usersSlice.actions;
// export the default reducer
export default usersSlice.reducer;













