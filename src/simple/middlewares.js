const usersMiddleware = store => next => action =>{
    if(typeof action === "function"){
        const {task} = action();
        if(task=="GET"){
            fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(json => 
            store.dispatch({type:"USERS_FETCHED", users:json})
            );
        }
        else if(task=="DELETE"){
            store.dispatch({type:"DELETE"});
        }
    }
    else{
        next(action);
    }
     
   }
  export default usersMiddleware;