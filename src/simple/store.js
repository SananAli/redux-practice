import { combineReducers } from "redux";
import {configureStore, getDefaultMiddleware} from "@reduxjs/toolkit";
// import reducers from "./index";
import reducers from "../redux-thunk/users"
// import usersMiddleware from "./middlewares";
import usersMiddleware from "../redux-thunk/usersMiddleware";
// const middlewareEnhancer = applyMiddleware(usersMiddleware)
const rootReducer = combineReducers({
    users:reducers,
})
const store = configureStore({
    reducer:rootReducer,
    middleware: [
        ...getDefaultMiddleware({ serializableCheck: {  } }),
        usersMiddleware
    ]
})
export default store;